<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.01.2017
 * Time: 18:18
 */
$Hello = 'Hello!';

echo 'Simple single-quoted string with string variable $Hello' . PHP_EOL;
echo 'Single-quoted string with characters escaping   \'' . $Hello . '\'';
echo PHP_EOL;
echo "Simple double-quoted string with variable {$Hello}" . PHP_EOL;
echo "Double-quoted string with escape sequences '{$Hello}'\n";

echo PHP_EOL;
echo <<<HEREDOC
String with 
            HereDoc syntax 
                           with variable: "{$Hello}"
HEREDOC;

echo PHP_EOL . PHP_EOL;
echo <<<'NOWDOC'
String with 
            NowDoc syntax 
                           with variable:"{$Hello}"
NOWDOC;
