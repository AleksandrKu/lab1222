<?php
/* Lab 1.2.2.3
Create console application that proceed math operations on different data types and compare float numbers*/
$integer = 7;
$stringNumber = '5';
$string = 'The best ';
$stringNumberEnd = 'Number 5';
$stringNumberBefore = '10 things';
$float = 3.14;

echo "Integer {$integer} + String of number '{$stringNumber}' = ";
var_dump($integer + $stringNumber); // int(12)
echo "String '{$string}' + Integer {$integer} = ";
var_dump($string + $integer); // int(7)
echo "String with number at the end '{$stringNumberEnd}' + Integer {$integer} = ";
var_dump($stringNumberEnd + $integer); // int(7)
echo "String with number before '{$stringNumberBefore}' + Integer {$integer} = ";
var_dump($stringNumberBefore + $integer); // int(17)
echo "String  '{$string}' + string '{$stringNumberBefore}' = ";
var_dump($string + $stringNumberBefore); // int(10)
echo "String  '{$string}' + string '{$stringNumberEnd}' = ";
var_dump($string + $stringNumberEnd); // int(0)
echo "Float {$float} + Integer {$integer} = ";
var_dump($float + $integer); //float(10.14)

echo '------------------' . PHP_EOL;

$float1 = 1.3;
$float2 = 0.1;
$sum = $float1 + $float2; // must be 1.4
if ($sum == 1.4 ) { echo "Sum float numbers  '{$float1} + {$float2}' equal float number '1.4'"; } else { echo "Sum float numbers  '{$float1} + {$float2}' does not equal float number '1.4'";  }
echo PHP_EOL;
echo "Result execution 'var_dump(($float1 + $float2) == 1.4)' is ";
var_dump(($float1 + $float2) == 1.4);

