<?php
/**
 * Created by PhpStorm.
 * User: Aleksander Kunup
 * Date: 02.02.2017
 * Time: 11:28
 */
echo 'The directory path: ' . __DIR__ . PHP_EOL;
echo 'The full path and filename: ' .  __FILE__ . PHP_EOL;
echo 'Current line number: ' .  __LINE__ . PHP_EOL;
echo "\n-----------------------------------------------\n";

// const CONSTANT = "For test!";
// Constant CONSTANT_NEW not exist
defined('CONSTANT_NEW') || define('CONSTANT_NEW', 'Mainakad'); // Constant CONSTANT_NEW is declared.
echo PHP_EOL.CONSTANT_NEW;
echo "\n-----------------------------------------------\n";

const CONSTANT_EXAMPLE = "Brainacad";
if (defined('CONSTANT_EXAMPLE')) {
    echo 'Constant CONSTANT_EXAMPLE = \''.CONSTANT_EXAMPLE.'\'  is declared';
} else {
    echo 'Constant CONSTANT_EXAMPLE is not declared';
}

