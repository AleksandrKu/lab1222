<?php
/* Lab 1.2.2.2 */
/*change type of variables  boolean to integer*/
echo 'false boolean: '.$boolean = false;
echo PHP_EOL;
echo 'var_dump: $boolean = false ';
var_dump($boolean);
echo "false boolean in integer: ".$boolean = intval($boolean); //boolean to integer false
echo PHP_EOL;
echo "true boolean: ".$boolean = true;
echo "\ntrue boolean in integer:"; echo $boolean = (int)($boolean); //boolean to integer true
echo PHP_EOL;
echo PHP_EOL;

/*change type of variables  integer to boolean */
$second = 10;
$second2 = 0;
$second3 = -10;
echo"Integer 10 to boolean:";   echo $second = (bool)$second; echo PHP_EOL;
echo"Integer 0 to boolean:"; echo $second2 = (bool)$second2; echo PHP_EOL;
echo"Integer -10 to boolean:"; echo $second3 = (bool)$second3; echo PHP_EOL;
echo PHP_EOL;

/*change type of variables  integer to float and conversely */
$float = 2.5;
echo"Float 2.5 to integer:";   echo $float = (int)$float; echo " var_dump:";  var_dump($float);
echo"Integer $float to float:";   echo $float = (float)$float; echo " var_dump:";  var_dump($float);
echo PHP_EOL;

/*change type of variables  string to integer */
$string = "Hello world!";
echo"'Hello world!' to integer: "; var_dump((int)$string);
$string2 = "5 friends";
echo"5 friends  to integer: "; var_dump((int)$string2);
$string3 = "3 boys and 4 girls";
echo"3 boys and 4 girls: "; var_dump((int)$string3);
echo PHP_EOL;

/*change type of variables  array to string and conversely */
$array = array ( 1,2,3,4,5);
echo '$array (1,2,3,4,5) to string: ';
var_dump((array)$array);
echo PHP_EOL;
$string4 = 'Hello world!';
echo 'String "Hello world" to array: ';
var_dump((array)$string4);
echo PHP_EOL;
